#!/usr/bin/env python

import sys

# for some reason ast isn't working for me
#from ast import make_tuple,literal_eval

#hist_file = '/fslhome/reedcw/compute/steam/final/fhist.txt'
hist_file = 'fhist.txt'


def parse_hist():
    with open(hist_file) as f:
        return [ line.split()[0] for line in f.readlines() ]

def tuplify(thing):
    # get rid of parens
    thing = thing.replace('(','').replace(')','')

    # turn the tuple into a tuple
    parts = thing.split(',')
    return (int(parts[0]), int(parts[2]))

def parse_list(line):
    # remove space between commas and split
    parts = line.replace(', ',',').split()
    user_id = parts[0]
    size = int(parts[2])

    # we don't care about it if it doesn't have friends
    if size > 0:

        # this is how you would parse if you cared to
        ## remove brackets
        #parts[1] = parts[1].replace('[','').replace(']','')

        ## create list from string (because ast isn't working
        #friends_list = [ tuplify(entry) for entry in parts[1].split('),(') ]


        # emit the result
        print '%d\t%s' % (size, user_id)

def main():
    hist = parse_hist()
    low = hist[1:3]
    high = hist[7:]
    for line in sys.stdin:
        parse_list(line)


if __name__ == '__main__':
    main()



#76561197960265730       [(76561197974593417, friend, 1347313517),(76561198010062752, friend, 1356072070),(76561198057927861, friend, 1349541361),(76561198064472375, friend, 1338830143),(76561198064472489, friend, 1338831013),(76561198064514762, friend, 1338830209),(76561198064515306, friend, 1338830782),(76561198064516438, friend, 1338830173)] 8
#76561197960265740       [(76561197960265731, friend, 0),(76561197960265738, friend, 0),(76561197960265743, friend, 0),(76561197960265744, friend, 0),(76561197960265747, friend, 0),(76561197960265749, friend, 0),(76561197960265838, friend, 0),(76561197960269040, friend, 0),(76561197960270258, friend, 0),(76561197960272813, friend, 0),(76561197960287930, friend, 0),(76561197960299622, friend, 0),(76561197960434622, friend, 1361927255),(76561197960435530, friend, 0),(76561197960549564, friend, 0),(76561197960563532, friend, 0),(76561197961301890, friend, 0),(76561197962783665, friend, 1307831409),(76561197962844216, friend, 0),(76561197963156385, friend, 0),(76561197963997393, friend, 0),(76561197964360086, friend, 0),(76561197964770089, friend, 0),(76561197968452293, friend, 0),(76561197968459473, friend, 1328296931),(76561197968575517, friend, 0),(76561197968852820, friend, 0),(76561197968949516, friend, 0),(76561197969013812, friend, 1342463819),(76561197969321754, friend, 1314472063),(76561197969518075, friend, 1324683110),(76561197970245402, friend, 1348025208),(76561197970259091, friend, 1230878801),(76561197970285523, friend, 1226365191),(76561197970323416, friend, 1307831400),(76561197970530062, friend, 1319659259),(76561197970565175, friend, 0),(76561197970576753, friend, 0),(76561197970892150, friend, 0),(76561197971049296, friend, 1297396977),(76561197971292977, friend, 0),(76561197971373352, friend, 1311985053),(76561197974593417, friend, 1285254173),(76561197975593810, friend, 0),(76561197978236369, friend, 0),(76561197980258575, friend, 1347301219),(76561197980632230, friend, 0),(76561197984981409, friend, 1309234363),(76561197985607672, friend, 0),(76561197988042654, friend, 0),(76561197989833880, friend, 0),(76561197991455313, friend, 0),(76561197992219796, friend, 1227404857),(76561197992681877, friend, 1340149900),(76561197992927490, friend, 0),(76561197992972642, friend, 0),(76561198000974417, friend, 1234659190),(76561198010062752, friend, 1343676959),(76561198021990498, friend, 1271093155),(76561198024402255, friend, 1273443708),(76561198027792863, friend, 1279588643),(76561198028573551, friend, 1328723498)] 62
#76561197960265750       [] 0
#76561197960265770       [] 0
#76561197960265780       [] 0
