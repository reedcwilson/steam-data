#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>

#include <string.h>

#include "jsmn.h"

using namespace std;

void parse(char* buffer);
string print_token(char**, jsmntok_t);

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        cout << "Usage: " << argv[0] << " <json_file_name>" << endl;
        return -1;
    }
    //Read file in as pure binary
    //http://stackoverflow.com/a/6755132

    //open the file
    char* path = argv[1];
    ifstream is;
    is.open(path, ifstream::binary);

    //determine file size
    is.seekg(0, is.end);
    size_t size = is.tellg();
    is.seekg(0, is.beg);

    //create file-sized buffer
    char* buffer = (char*)malloc(sizeof(char)*size);

    //read the file
    is.read(buffer, size);

    //close the archive
    is.close();

    //parse the buffer contents
    parse(buffer);
  
    //free buffer memory
    free(buffer);
}

string print_token(char** buffer, jsmntok_t t)
{
	char* substr;
	cout << "start: " << t.start << " end: " << t.end;
	return "";
	//strncpy(substr, (*buffer), 4);
	//ostringstream oss;
	//oss << "type: " << t.type << ", element: " << substr <<  ", children: " << t.size;
	//return oss.str();
}

void parse(char* buffer)
{
    //cout << buffer << endl; // currently just printing buffer contents
    int i, r;
    jsmn_parser p;
    jsmntok_t t[10000];
    jsmn_init(&p);
    r = jsmn_parse(&p, buffer, strlen(buffer), t, sizeof(t)/sizeof(t[0]));
    if (r < 0) 
    {
        printf("Failed to parse JSON: %d\n", r);
        return;
    }
    for (i = 0; i < sizeof(t)/sizeof(t[0]); ++i) 
    {
        cout << print_token(&buffer, t[i]) << endl;
    }
}

/* vim: set sw=4 ts=4 tw=4 noet : */
