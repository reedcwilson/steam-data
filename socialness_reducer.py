#!/usr/bin/env python

from operator import itemgetter
import sys

current_number = None
current_list = []
number = None

def print_output(current_number, current_list):
    # print result and remove duplicates
    print '%s\t%s' % (current_number, str(list(set(current_list))))


for line in sys.stdin:
    line = line.strip()
    number, user_id = line.split('\t', 1)

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: number) before it is passed to the reducer
    if current_number == number:
        current_list.append(user_id)
    else:
        if current_number:
            # write result to STDOUT
            print_output(current_number,current_list)
        current_list = [user_id]
        current_number = number

# do not forget to output the last number if needed!
if current_number == number:
    print_output(current_number,current_list)

# vim set sw=4, sts=4, ts=4
