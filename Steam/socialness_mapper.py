#!/usr/bin/env python

import sys

# for some reason ast isn't working for me
#from ast import make_tuple,literal_eval

hist_file = '/fslhome/reedcw/compute/steam/final/fhist.txt'
#hist_file = 'fhist.txt'


def parse_hist():
    with open(hist_file) as f:
        return [ line.split()[0] for line in f.readlines() ]

def tuplify(thing):
    # get rid of parens
    thing = thing.replace('(','').replace(')','')

    # turn the tuple into a tuple
    parts = thing.split(',')
    return (int(parts[0]), int(parts[2]))

def parse_list(line):
    # remove space between commas and split
    parts = line.replace(', ',',').split()
    user_id = parts[0]
    size = int(parts[2])

    # we don't care about it if it doesn't have friends
    if size > 0:

        # this is how you would parse if you cared to
        ## remove brackets
        #parts[1] = parts[1].replace('[','').replace(']','')

        ## create list from string (because ast isn't working
        #friends_list = [ tuplify(entry) for entry in parts[1].split('),(') ]


        # emit the result
        print '%d\t%s' % (size, user_id)

def main():
    hist = parse_hist()
    low = hist[1:3]
    high = hist[7:]
    for line in sys.stdin:
        parse_list(line)


if __name__ == '__main__':
    main()
