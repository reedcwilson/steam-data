#!/usr/bin/env python

import sys

threshold = 10

for line in sys.stdin:
  game, number = line.strip().split()
  number = int(number)
  if number > threshold:
    print game, number


# vim set sw=4, sts=4, ts=4
