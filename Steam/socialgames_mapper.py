#!/usr/bin/env python

import sys

file = '/fslhome/reedcw/compute/steam_social_out/part-00000'

user_ids = []
with open(file) as f:
  # TODO: change portion (1:3 for low, 7: for high)
  for line in f.readlines()[1:3]:
  #for line in f.readlines()[7:]:
  #for line in f.readlines()[250:257]:
    if line:
      line = line.replace(', ',',') # remove space
      parts = line.replace("'",'').split() # split and remove apostrophes
      parts[1] = parts[1].replace('[','').replace(']','') # remove brackets
      user_ids.extend([ int(user_id) for user_id in parts[1].split(',') ])
user_ids = set(user_ids) # get rid of any duplicates

dict = {}

for line in sys.stdin:
  user_id, games_list, number = line.split()
  # if they don't have games then stop right there
  number = int(number)
  if number > 0:
    user_id = int(user_id)
    if user_id in user_ids:
      games_list = [ int(game) for game in games_list.replace('[','').replace(']','').split(',') ]
      for game in games_list:
        if game not in dict:
          dict[game] = 1
        else:
          dict[game] += 1

for key in dict:
  print key, dict[key]

