#!/usr/bin/env python

import sys
import json

data = '{"response":{"game_count":25,"games":[{"appid":10180,"playtime_forever":285},{"appid":10190,"playtime_forever":2886},{"appid":12840,"playtime_forever":7},{"appid":42700},{"appid":42710,"playtime_forever":2142},{"appid":10},{"appid":20},{"appid":30},{"appid":40},{"appid":50},{"appid":60},{"appid":70},{"appid":130},{"appid":260},{"appid":320},{"appid":340,"playtime_forever":5},{"appid":220},{"appid":240,"playtime_forever":4},{"appid":80},{"appid":100},{"appid":280},{"appid":300},{"appid":360},{"appid":42680},{"appid":42690,"playtime_forever":60}]}} \
{"response":{"game_count":26,"games":[{"appid":500,"playtime_forever":223},{"appid":10180,"playtime_forever":1223},{"appid":10190,"playtime_forever":10629},{"appid":42700,"playtime_forever":212},{"appid":42710,"playtime_forever":10257},{"appid":10,"playtime_forever":69},{"appid":20},{"appid":30},{"appid":40},{"appid":50},{"appid":60},{"appid":70},{"appid":130},{"appid":80},{"appid":100},{"appid":260},{"appid":320},{"appid":340},{"appid":220},{"appid":240},{"appid":400},{"appid":42680,"playtime_forever":185},{"appid":42690,"playtime_forever":13198},{"appid":212910},{"appid":202970},{"appid":202990,"playtime_2weeks":430,"playtime_forever":4870}]}} \
{"response":{}} \
{"response":{}} \
{"response":{}} \
{"response":{"game_count":15,"games":[{"appid":10,"playtime_forever":47},{"appid":20},{"appid":30},{"appid":40},{"appid":50},{"appid":60},{"appid":70},{"appid":130},{"appid":80},{"appid":100},{"appid":260},{"appid":320},{"appid":340},{"appid":220},{"appid":240,"playtime_forever":307}]}} \
{"response":{}}'

#for line in sys.stdin:
player = json.loads(data)
for game in player.games:
    print game


# vim et sw=4 ts=4 sts=4
