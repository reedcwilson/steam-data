#!/usr/bin/env python

from pylab import plot,show,scatter
from numpy import array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq
from collections import OrderedDict
#import matplotlib.pyplot as plt

games = OrderedDict({})
# merge our two results into one data set
with open('part-00000') as users_file:
    for line in users_file.readlines():
        parts = line.split()
        # add to list with normalized (10000)
        games[int(parts[0])] = [ float(parts[1])/10000 ]
        #games[int(parts[0])] = [ float(parts[1]) ]
with open('average_time_played.txt') as average_file:
    for line in average_file.readlines():
        parts = line.split()
        if int(parts[0]) in games:
            # add to list with normalized (100)
            games[int(parts[0])].append(float(parts[1])/100)
            #games[int(parts[0])].append(float(parts[1]))

# pull out our coordinates to give to kmeans and vq
data = array([ games[game] + array([0.0, 0.0]) for game in games ])

# computing K-Means
centroids,_ = kmeans(data,10)
# assign each sample to a cluster
idx,_ = vq(data,centroids)

# use the idx to map clusters back onto our data
items = []
for i, item in enumerate(idx):
    items.append((games.items()[i], item))

# print out games
newlist = []
for i, item in enumerate(items):
    newlist.append((item[0][0], item[0][1][0], item[0][1][1], item[1]))
    print newlist[i]


x = [ item[1] for item in newlist ]
y = [ item[2] for item in newlist ]
c = [ item[3] for item in newlist ]
scatter(x, y, c=c, alpha=0.5)


# some plotting using numpy's logical indexing
#plot(data,'ob',markersize=1)
#plot(data[idx==0,0],data[idx==0,1],'ob',
     #data[idx==1,0],data[idx==1,1],'or')
# plot centroids
plot(centroids[:,0],centroids[:,1],'sg',markersize=8)
show()
